---
layout: page
title: Benefits
weight: 2
---

## Benefits

* Unlimited PTO with minimums
* Professional development support and budget
* Reimbursable coworking fees and external office space
* Budget for office equipment and workspace supplies
* [Team offsites](/company/all-remote#meltano-assemble)
* Work with a global, distributed team
* Flexible working hours
* Access to world class founders, investors and mentors

### Flexible working hours

Because Meltano is a fully remote and distributed team, we don't have set working hours. Team members are encouraged to set their own work schedule, depending on their needs and working style. We understand that some people work best at certain times of the day, so please work when you're at your best. Also, if you need to take a break during the day, have a doctor's appointment, or need to pick up your kids, please feel empowered to readjust your schedule. Please note that there may be times when you'll need to adjust your schedule (e.g. a necessary synchronous meeting). 

#### Taking breaks during the day

Whether you go for a walk, take a nap, or watch your favorite show, you're enouraged to take breaks during the day. [Research](https://www.bbc.com/worklife/article/20190312-the-tiny-breaks-that-ease-your-body-and-reboot-your-brain) has found that taking breaks - even for a few minutes - improves productivity, creativity, and concentration. Don't ever feel guilty when you need time away from your computer. 

### Paid time off

Meltano has an unlimited time off policy, and we encourage team members to take the time they need to rest and recharge. 

We recommend every team member takes a minimum 30 days off a year, and we have recommended minimums for various life events, including:

* Minimum 15 days for compassionate leave 
* Minimum 15 days for victims of domestic violence
* Minimum 15 days for burnout

To log time off, team members should use [PTO by Roots](/company/tech-stack/#pto-by-roots) in Slack (under Apps).
Follow the instructions in [Tracking Time Off](/peopleops/calendars#tracking-time-off) to indicate your PTO.

### Visiting grant

**Note**: To keep team members and our communities safe, we've temporarily suspended the visiting grant until travel is less of a risk around the world. We'll provide updates to team members and will regularly review this policy over the course of the pandemic.

Meltano is an all-remote company with team members all over the world. If you want to visit other team members to get to know them, Meltano will assist with travel expenses (e.g. flights, trains, parking, car rental, and ground transportation to/from airport) for a total of up to $150 for every team member that you visit. Please note lodging is excluded. To be clearer, if you meet 2 team members during your visit, the maximum limit of your visiting grant could be $300 ($150*2). You don't need to work on the same project or team. We'd like you to get to know each other and your roles at Meltano, so we encourage you to discuss work for at least part of the time you're meeting.

Please note: The visiting grant applies to transportation costs and not other expenses related to travel (e.g. lodging, food). It may be acceptable to cover a meal, however, if the meeting is pre-announced to other people in the region to encourage as many of them to attend as possible.

To claim the grant, include a line item on your expense report or invoice along with the receipt of your flights, trains, and/or transportation to/from the airport with a list of the team members you visited. The expense report may be submitted during the first month of travel or up to 3 months after your trip has concluded. That said, if it's more frugal to book and expense your travel further in advance, please do so.

The visiting grant is inspired by Douwe's trip around the world during his time at GitLab. Douwe met [49 colleagues in 20 cities, in 14 countries, on five continents, in 6 months](https://about.gitlab.com/blog/2017/01/31/around-the-world-in-6-releases/). 

#### Significant life event grant

Recognizing that team members may wish to share significant events in each other's lives, such as weddings or civil partnerships, Meltano will assist with travel expenses to attend these events. This grant works the same way as the, except the reimbursement limit is $300 per team member you visit at an event.

### Family and Friends Day

Inspired by GitLab's [Family and Friends Day](https://about.gitlab.com/company/family-and-friends-day/), Meltano has made Family and Friends Day a regular part of the culture. On these days, we will close the doors to the Meltano virtual office, reschedule all meetings, and have a publicly visible shutdown. Taking the day off is strongly encouraged if your role allows it. If you are on call or need to work during the scheduled Friends and Family Day, please select another day that works best for your schedule.

#### Upcoming Family and Friends Days

* 2021-12-27
* 2022-01-03
* 2022-02-14
* 2022-03-21

These dates are tracked in the [Team Meetings Calendar](calendars). 
Meltano team members should mark these days off using [PTO by Roots](/company/tech-stack/#pto-by-roots).

In line with our Paid Time Off policy, we encourage Meltano team members to continue to take additional days off, as needed. 
Family and Friends Day is a reminder to do this.

---
layout: page
title: Compensation
weight: 2
---

Compensation for each role is a combination of salary, equity, and benefits.

## Salary

The salary for a role is determined using market data from [Option Impact](https://www.optionimpact.com/) that takes into account the stage of our company (Seed Funding Only), the industry we're in (Enterprise), and the level of the role (Junior, Intermediate, Senior, etc).

[Where you work from](/company/all-remote#where-we-hire) is also a factor.
We aim to pay at the top or above market everywhere by
dividing the world up into a small number of regions,
and using this same market data to determine a salary that is competitive in the entire region.

Specifically, we determine the lower and upper bounds for regional salaries by
taking the top-of-the-market (75th-90th percentile) salaries for the role in San Francisco, and
multiplying them with a region-specific factor that is determined by comparing the SF salaries with salaries in the region's major cities (that represent its top-of-the-market).

The region-specific factors that we have calculated so far are:

- United States & Canada: 100%
- Europe: 70%
  - Switzerland: 100%
- Latin America: 50%

If your region is not listed here, please get in touch. We will add factors for more regions soon.

If a single city or country in a region represents a significant outlier compared to other major cities in the same region (like Zurich, Switzerland in Europe), this area gets its own factor, and is not taken into account to determine the region-wide factor. If you believe your city or country qualifies, please get in touch. Generally, we prefer to raise the factor for an entire region if there are not one but multiple outliers, and they represent a significant part of the region's major cities.

For roles at the Director/Head level and up, we pay US salaries regardless of the region,
as the top-of-the-market talent pool at these levels is small and as a result regional differences in salary are as well.

## Equity

As an early-stage company, we expect every team member to have a huge impact on our success. We want to make sure that everyone has a financial stake in the success and that contributions are rewarded.

The equity (stock options) for a given role and level is determined using the same market data used for the [salary](#salary), but there is no difference between regions.

Specifically, we divide the 75th percentile Gross Equity Value by the latest valuation of the company to determine the appropriate ownership percentage, which is then multiplied by the total number of shares to determine the number of options to be granted.

Options vest monthly over 4 years, with a 1-year cliff. Early exercise is allowed in countries where this has potential tax benefits, like the US.

## Benefits

See the page on [Benefits](benefits).

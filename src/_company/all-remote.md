---
layout: page
title: All-Remote
weight: 2
---

Meltano is all-remote, which means we don't have any offices,
hire the best talent from [all around the world](#where-we-hire), and
rely heavily on [asynchronous communication](/company/communication#asynchronous-communication) so that people can work where and when fits them best.

# We learned from the best

As Meltano [came out of](/company/#history) [GitLab](https://about.gitlab.com), one of the world's largest all-remote companies,
our approach to remote work is modeled closely after theirs.
GitLab's [Remote Playbook](https://about.gitlab.com/company/culture/all-remote/) and [public handbook](https://about.gitlab.com/handbook/) are invaluable resources to learn about remote work at scale, and much of what's described there in terms of processes and best practices can be assumed to apply to Meltano as well.

Of course, we move things over to our own handbook (which you are reading right now) when we can adapt them to be more appropriate for our size and unique culture.

# Meltano Assemble

Every nine months, we get the whole team together somewhere on the planet for Meltano Assemble.

Assemble 2021 took place in beautiful Mexico City.
To learn more about our adventures in Mexico and Assemble in general, check out [the roundup on our blog](https://meltano.com/blog/assemble-2021-mexico-city-roundup/).

# Where we hire

Our [team](https://meltano.com/team) is currently distributed over the United States, Mexico, and the United Kingdom.
We hope to keep growing that list until we cover all continents and time zones.

To hire globally, we use [Remote](https://remote.com), who currently provide service in over 50 countries with dozens being added every year.
If your country shows as "Available" in Remote's [Country Explorer](https://remote.com/country-explorer?layout=list), it is very likely that we will be able to hire you there.

In terms of time zones, we require there to be some overlap with your manager for 1:1s, but you are generally free to arrange your working hours however works for you.
Some community-facing roles require you to be in a specific time zone or region, in which case this will be specified in the job description.

Check out our section on [compensation](/peopleops/compensation) to learn how that is affected by where you live and work from.
